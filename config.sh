# Setup for base16
# git clone https://github.com/chriskempson/base16-shell.git ~/.config/base16-shell
# Setup for base16-gnome-terminal
git clone https://github.com/aaron-williamson/base16-gnome-terminal.git ~/.config/base16-gnome-terminal
.config/base16-gnome-terminal/color-scripts/base16-monokai.sh

# Install ruby
# Instead of sudo apt install rbenv
curl -fsSL https://github.com/rbenv/rbenv-installer/raw/HEAD/bin/rbenv-installer | bash

# Install Miniconda
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /tmp/miniconda.sh
bash /tmp/miniconda.sh -b
~/miniconda3/bin/conda init
~/miniconda3/bin/conda config --set auto_activate_base False
~/miniconda3/bin/conda update conda -y

# Install nerdfonts
mkdir -p ~/.local/share/fonts
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/UbuntuMono.zip -O /tmp/UbuntuMono.zip
unzip /tmp/UbuntuMono.zip ~/.local/share/fonts
fc-cache -fv


# Update the folders to lowercase and update
tee ~/.config/user-dirs.dirs << END
XDG_DESKTOP_DIR="$HOME/desktop"
XDG_DOWNLOAD_DIR="$HOME/downloads"
XDG_TEMPLATES_DIR="$HOME/templates"
XDG_PUBLICSHARE_DIR="$HOME/public"
XDG_DOCUMENTS_DIR="$HOME/documents"
XDG_MUSIC_DIR="$HOME/music"
XDG_PICTURES_DIR="$HOME/pictures"
XDG_VIDEOS_DIR="$HOME/videos"
END
