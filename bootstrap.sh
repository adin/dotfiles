#!/bin/bash
set -eu

# Set up soft links from files to their destination (in home directory)

# Note: /bin/bash is required for ~/.* expansion in loop below

# Can't use something like 'readlink -e $0' because that doesn't work everywhere
# And HP doesn't define $PWD in a sudo environment, so we define our own
case $0 in
  /*|~*)
    SCRIPT_INDIRECT="$(dirname $0)"
    ;;
  *)
    PWD="`pwd`"
    SCRIPT_INDIRECT="$(dirname $PWD/$0)"
    ;;
esac

BASEDIR="$(cd "$SCRIPT_INDIRECT" ; pwd -P)"

link_file () {
  local SOURCE=$1
  local TARGET=$2

  if [ -h "$TARGET" ]; then
    echo "Updating link : $TARGET"
    rm "$TARGET"
  elif [ -d "$TARGET" ]; then
    echo "Replacing directory: $TARGET (saving old version)"
    local SAVE_NAME="$TARGET.dotfiles.sav"
    if [ -e "$SAVE_NAME" ]; then
      SAVE_NAME="$SAVE_NAME.$(date +'%s')"
    fi
    mv "$TARGET" "$SAVE_NAME"
  elif [ -e "$TARGET" ]; then
    # if it exists but isn't a directory or a link,
    # assume it is a file. Doesn't seem worth it
    # to try to handle other things (e.g. unix sockets)
    # specially.
    echo "Replacing file: $TARGET"
    rm "$TARGET"
  else
    echo "Creating link: $TARGET"
  fi

  ln -s "$SOURCE" "$TARGET"
}


for i in "$BASEDIR"/*; do
  [ ! -d "$i" ] && continue

  CHECK="$(basename $i)"

  # Check if this folder is a config one
  if grep -ixq "\s*$CHECK\s*" "$BASEDIR"/config; then
    echo "Linking '$CHECK' into .config instead"
    BASEFILE="$HOME/.config/$CHECK"

    # link the folder and skip individual files
    link_file $i $BASEFILE
    continue
  fi

  # Ignore certain dirs
  if grep -ixq "\s*$CHECK\s*" "$BASEDIR"/ignore; then
    echo "Skipping directory from ignore list: $CHECK"
    continue
  fi

  # Link all files within this folder
  for j in "$i"/*; do
    FILEDIR="$(basename $(dirname $j))"
    FILE="$(basename $j)"

    CHECK="$FILEDIR/$FILE"
    # Check if this file is a config one
    if grep -ixq "\s*$CHECK\s*" "$BASEDIR"/config; then
      echo "Linking '$CHECK' into .config instead"
      BASEFILE="$HOME/.config/$FILE"
    else
      BASEFILE="$HOME/.$FILE"
    fi

    link_file $j $BASEFILE
  done
done

# Make a pass deleting stale links, if any
for i in ~/.*; do
  [ ! -h "$i" ] && continue

  # We have a link: Is it stale? If so, delete it ...
  # Since we can't use readlink, assume that if the link is
  # not pointing to a file or a directory that it is stale.
  if [ ! -f "$i" -a ! -d "$i" ]; then
    echo "Deleting stale link: $i"
    rm "$i"
  fi
done
