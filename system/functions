###
# SSH Helpers
###
_complete_ssh_hosts ()
{
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    comp_ssh_hosts=`cat ~/.ssh/known_hosts | \
                    cut -f 1 -d ' ' | \
                    sed -e s/,.*//g | \
                    grep -v ^# | \
                    uniq | \
                    grep -v "\[" ;
                    cat ~/.ssh/config | \
                    grep "^Host " | \
                    awk '{print $2}'
                    `
    COMPREPLY=( $(compgen -W "${comp_ssh_hosts}" -- $cur))
    return 0
}

complete -F _complete_ssh_hosts ssh


###
# Conda
###

# Execute the setup for initializing conda without writing it to the .bashrc
cinit() {
    # If conda points to the original file
    if [[ "$(type -t conda)" == "file" ]]; then
    conda_file=$(type -p conda)
    dir_path=${conda_file%/*/*}
    # Setup conda initialized
    __conda_setup="$('conda' 'shell.bash' 'hook' 2> /dev/null)"
    if [ $? -eq 0 ]; then
        eval "$__conda_setup"
    else
        if [ -f "${dir_path}/etc/profile.d/conda.sh" ]; then
            . "${dir_path}/etc/profile.d/conda.sh"
        else
            export PATH="${dir_path}/bin:$PATH"
        fi
    fi
    unset conda_file
    unset dir_path
    unset __conda_setup
    fi
}

cenv() {
read -r -d '' CENV_HELP <<-'EOF'
Usage: cenv [COMMAND] [FILE]

Detect, activate, delete, and update conda environments.
FILE should be a conda .yml environment file.
If FILE is not given, assumes it is environment.yml.
Automatically finds the environment name from FILE.

Commands:

  None     Activates the environment
  rm       Delete the environment
  up       Update the environment

EOF
    envfile="environment.yml"
    if [[ $# -gt 2 ]]; then
        >&2 echo "Invalid argument(s): $@";
        return 1;
    elif [[ $# == 0 ]]; then
        cmd="activate"
    elif [[ "$1" == "--help" ]] || [[ "$1" == "-h" ]]; then
        echo "$CENV_HELP";
        return 0;
    elif [[ "$1" == "rm" ]]; then
        cmd="delete"
        if [[ $# == 2 ]]; then
            envfile="$2"
        fi
    elif [[ "$1" == "up" ]]; then
        cmd="update"
        if [[ $# == 2 ]]; then
            envfile="$2"
        fi
    elif [[ $# == 1 ]]; then
        envfile="$1"
        cmd="activate"
    else
        >&2 echo "Invalid argument(s): $@";
        return 1;
    fi

    # Check if the file exists
    if [[ ! -e "$envfile" ]]; then
        >&2 echo "Environment file not found:" $envfile;
        return 1;
    fi

    # Get the environment name from a conda yml file
    envname=$(grep "name: *" $envfile | sed -n -e 's/name: //p')

    if [[ $cmd == "activate" ]]; then
        conda activate "$envname";
    elif [[ $cmd == "update" ]]; then
        >&2 echo "Updating environment:" $envname;
        conda activate "$envname";
        conda env update -f "$envfile"
    elif [[ $cmd == "delete" ]]; then
        >&2 echo "Removing environment:" $envname;
        conda deactivate;
        conda env remove --name "$envname";
    fi
}

ctmp(){
read -r -d '' CTMP_HELP <<EOF
Usage: ${FUNCNAME}

Activates and reverts to a clean state a base environment.
Or if ${FUNCNAME} doesn't exist, it creates it.
EOF
    if [[ $# -gt 1 ]]; then
        >&2 echo "Invalid argument(s): $@";
        return 1;
    elif [[ $# == 0 ]]; then
      : # nothing
    elif [[ "$1" == "--help" ]] || [[ "$1" == "-h" ]]; then
        echo "$CTMP_HELP";
        return 0;
    else
        >&2 echo "Invalid argument(s): $@";
        return 1;
    fi
  conda list -n __tmp >/dev/null 2>&1
  if [[ ! $? -eq 0 ]]; then
    conda create --name __tmp python=3 --yes >/dev/null 2>&1
  fi
  conda create --name tmp --clone __tmp --yes >/dev/null 2>&1
  con tmp
}
