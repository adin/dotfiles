# About

These are my dotfiles.  I'm using an addapted [bootstrap](https://github.com/jeffaco/dotfiles) from `jeffaco`'s project.

# Rationale

These dotfiles are linked into my user's `$HOME` through `bootstrap.sh`.  All the directories will be searched and their contents linked by prepending a period in front of them (hence, no need to dotify all the files).

There is an `ignore` directories file at the same level as `bootstrap.sh`.  That file contains a list of the names (paths relative to this repo directory) of the directories to be ignored in the linking process.  For instance, I have a set of system wide files that are used within `.bashrc` that shouldn't be linked into `$HOME`.

Similarly, there is a `config` file that lists directories to be linked within the `.config` folder instead of `$HOME`.

# File structure

Folders:
- `bash` holds the configuration files for bash terminal.
- `git` holds the git ignore and configuration files.
- `nautilus` holds the configuration for the nautilus file manager.
- `system` holds a set of system wide dotfiles to be loaded within `bashrc`.
- `vim` holds the configuration for vim.

Special files:
- `bootstrap.sh` is the installation file.
- `config` is a list of paths that should be installed within `~/.config` instead of the user's home (for example, nautilus config files).
- `config.sh` is an installation script that setups several tools.
- `ignore` is a list of directories to be ignored when `bootstrap`ing the dotfiles.

# Installing

Execute the `bootstrap` script

```
$ ./bootstrap.sh
```

## Local configuration

For local setups that require different configurations across machines, the dotfiles load local files (if present) from the user `$HOME`
- `~/.bash_aliases`: additional aliases loaded after the ones defined in the repo (see `system/aliases`).
- `~/.path`: additional `$PATH` setup loaded after `system/path`, but it is cleaned together (remove duplicates).  Use `prepend-path` function instead of manually adding to path to check if the file structure exist to add it.